<%-- 
    Document   : liste-personne.jsp
    Created on : 7 nov. 2022, 15:22:05
    Author     : steven
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="modele.Personne"%>
<%@page import="modele.ModelView"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% ArrayList<Personne> list = (ArrayList<Personne>) request.getAttribute("liste"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <% 
            for (int i = 0; i < list.size(); i++) { %>
            <h3><%= list.get(i).getNom() %> <%= list.get(i).getAge() %> ans</h3>
        <% } %>
    </body>
</html>
