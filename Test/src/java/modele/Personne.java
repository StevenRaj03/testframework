package modele;

import annotation.Url;
import java.util.ArrayList;
import java.util.HashMap;

public class Personne {
    private String nom;
    private int age;
    
    public Personne() {
        //default constructor
        super();
    }
    
    public Personne(String nom, int age) {
        super();
        this.nom = nom;
        this.age = age;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getNom() {
        return nom;
    }

    @Url(valeur = "personne-all.do")
    public ModelView lister() {
        /*
        *   this is a static list of personne
        */
        ArrayList list = new ArrayList();
        list.add(new Personne("Rakoto", 21));
        list.add(new Personne("Rabe", 41));
        list.add(new Personne("Rasoa", 30));
        list.add(new Personne("Rajao", 19));
        list.add(new Personne("Rabary", 18));
        ModelView rep = new ModelView();
        rep.setUrl("liste_personne.jsp");
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("liste", list);
        rep.setData(map);
        return rep;
    }
    
    @Url(valeur = "personne-add.do")
    public ModelView add() {
        /*
        *   this just print the object
        */
        ModelView rep = new ModelView();
        rep.setUrl("add_personne.jsp");
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("personne", this);
        rep.setData(map);
        System.out.println(this.getNom() + " " + this.getAge() + " ans ajouté(e) avec succès.");
        return rep;
    }
    
}
