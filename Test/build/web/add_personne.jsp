<%-- 
    Document   : add_personne.jsp
    Created on : 9 nov. 2022, 11:19:33
    Author     : steven
--%>

<%@page import="modele.Personne"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% Personne p = (Personne) request.getAttribute("personne"); %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ajouter personne</h1>
        <form action="<%= request.getContextPath() %>/personne-add.do" method="POST">
            <div><input type="text" name="nom" placeholder="Nom"/></div>
            <div><input type="number" name="age" placeholder="Age"/></div>
            <div><input type="submit" value="Ajouter"/></div>
        </form>
        <%
            if (p.getNom() != null) {
                out.print("<p style=\"color:green\">" + p.getNom() + " " + p.getAge() + " ans ajouté(e) avec succès.</p>");
            }
        %>
    </body>
</html>
